'use strict'

require('dotenv-safe').load({ allowEmptyValues: true })
require('events').EventEmitter.defaultMaxListeners = 15

const cors = require('cors')
const Express = require('express')
const bodyParser = require('body-parser')
const Helmet = require('helmet')
const APIResponse = require('../lib/APIResponse')
const config = require('../config')

const app = new Express()
app.set('env', config.env)
app.set('views', '../../app/views')
app.set('view engine', 'jade')

app.disable('x-powered-by')

app.use(Helmet())
app.use(cors({
  origin: '*',
  optionsSuccessStatus: 200
}))
app.enable('trust proxy')
app.use(Express.static('../../app'))
app.use(bodyParser.json({
  limit: '20mb',
  extended: true,
  verify: (req, res, rawBody) => {
    req.rawBody = rawBody
  }
}))
app.use(bodyParser.urlencoded({
  limit: '20mb',
  extended: false
}))
app.use(bodyParser.json())
app.set('json space', 0)
app.use(APIResponse.bind())
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')

  next()
})

app.options('*', cors())

require('../config/models')
require('../config/routes')(app)
require('./database').connect()

app.use((err, req, res, next) => {
  console.error(err, 'ExpressJS error')
  res.status(err.statusCode || 500).json(err)
})

module.exports = app
