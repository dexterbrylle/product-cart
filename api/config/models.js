'use strict'

console.log(`Models initialized.`)

module.exports = {
  Cart: require('../components/cart/cart.model'),
  Customer: require('../components/customer/customer.model'),
  Product: require('../components/product/product.model')
}
