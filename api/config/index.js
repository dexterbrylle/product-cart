'use strict'

const path = require('path')

const config = {
  env: process.env.NODE_ENV || 'development',
  root: path.join('../', __dirname),
  port: process.env.PORT || 5000,
  ip: process.env.IP || '0.0.0.0',
  mongo: {
    uri: process.env.MONGO_URI,
    user: process.env.MONGO_USER,
    pass: process.env.MONGO_PASS,
    auth: process.env.MONGO_AUTH
  }
}

module.exports = config
