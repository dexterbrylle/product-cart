'use strict'

const config = require('./index')
const mongoose = require('mongoose')

class Database {
  connect () {
    mongoose.Promise = global.Promise

    mongoose.connect(config.mongo.uri, {
      'user': config.mongo.user,
      'pass': config.mongo.pass,
      useNewUrlParser: true,
      autoReconnect: true,
      authSource: 'admin',
      poolSize: 25,
      reconnectTries: 25,
      reconnectInterval: 5000,
      connectTimeoutMS: 100000,
      socketTimeoutMS: 180000
    })
      .catch(e => {
        console.error(`MongoDB Error: ${e}`)
      })

    process.on('SIGINT', this.cleanup)
    process.on('SIGTERM', this.cleanup)
    process.on('SIGHUP', this.cleanup)
  }

  cleanup () {
    mongoose.connection.close(() => {
      console.log(`MongoDB connection closed.`)
      process.exit(0)
    })
  }
}

module.exports = new Database()
