'use strict'

console.log(`Routes initialized.`)
module.exports = app => {
  app.get('/', (req, res, next) => {
    res.render('index', {
      title: 'Express'
    })
  })

  // app.use('/api/cart', require('../components/cart/cart.route'))
  // app.use('/api/customer', require('../components/customer/customer.route'))
  // app.use('/api/product', require('../components/product/product.route'))
}
