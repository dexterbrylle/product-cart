'use strict'

class APIResponse {
  static bind () {
    return (req, res, next) => {
      res.api = new APIResponse(res)
      next()
    }
  }

  constructor (res) {
    this.res = res

    this.getMany = this.getMany.bind(this)
    this.getOne = this.getOne.bind(this)
    this.getOneOrNotFound = this.getOneOrNotFound.bind(this)
    this.create = this.create.bind(this)
    this.notFound = this.notFound.bind(this)
    this.error = this.error.bind(this)
    this.badRequest = this.badRequest.bind(this)
    this.successMessage = this.successMessage.bind(this)
  }

  getMany (data) {
    this.res
      .status(200)
      .json({
        'result': APIResponse.ResultCode.OK,
        'data': data,
        'count': data.length
      })
  }

  getOne (data) {
    this.res
      .status(200)
      .json({
        'result': APIResponse.ResultCode.OK,
        'data': data || null,
        'count': (data) ? 1 : 0
      })
  }

  getOneOrNotFound (data) {
    if (data) {
      return this.res.api.getOne(data)
    } else {
      return this.res.api.notFound()
    }
  }

  create (data) {
    let data = (Array.isArray(data)) ? data[0] : data

    this.res
      .status(201)
      .json({
        'result': APIResponse.ResultCode.OK,
        'data': data,
        'count': (data) ? 1 : 0
      })
  }

  notFound () {
    this.res
      .status(404)
      .json({
        'result': APIResponse.ResultCode.NOT_FOUND,
        'data': null,
        'count': 0
      })
  }

  error (err) {
    this.res
      .status(500)
      .json({
        'result': APIResponse.ResultCode.INTERNAL_SERVER_ERROR,
        'message': err.message,
        'stack': (err.stack) ? err.stack.split('\n') : undefined,
        'errors': err.errors || undefined
      })
  }

  badRequest (data, msg) {
    this.res
      .status(400)
      .json({
        'result': APIResponse.ResultCode.BAD_REQUEST,
        'message': msg,
        'resource': data
      })
  }

  successMessage (text) {
    this.getOne({
      'result': APIResponse.ResultCode.OK,
      'message': text
    })
  }
}

APIResponse.ResultCode = {
  OK: 'OK',
  NOT_FOUND: 'NOT_FOUND',
  BAD_REQUEST: 'BAD_REQUEST',
  INTERNAL_SERVER_ERROR: 'INTERNAL SERVER ERROR',
  CONFLICT: 'CONFLICT'
}

module.exports = APIResponse
