'use strict'

const app = require('./api/config/express')
const config = require('./api/config')
const http = require('http')

const port = normalizePort(config.port)

app.set('port', port)

const server = http.createServer(app)

server.on('error', error => {
  console.error(`Server runtime error: ${error}`)
  if (error.syscall !== 'listen') {
    throw error
  }

  if (error.code === 'EACCES') {
    console.error(`${port} requires elevated privileges.`)
    process.exit(1)
  }

  if (error.code === 'EADDRINUSE') {
    console.error(`${port} already in use.`)
    process.exit(1)
  }
})

server.on('listening', () => {
  console.log(`API Server running on port  ${server.address().port}`)
})

function normalizePort (p) {
  let port = parseInt(p, 10)

  if (isNaN(port)) {
    return p
  }

  if (port >= 0) {
    return port
  }

  return false
}

process.on('SIGTERM', () => {
  console.info(`API server terminated on port ${port}`)
  server.close(() => {
    process.exit(0)
  })
})

module.exports = server
